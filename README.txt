General notes
=====================================================

This module provides a block and page with a composite image of all the 
avatars of a site. It serves to promote a sense of community around a site 
while avoiding the unnecessary database queries and image calls that would be 
caused by a PHP-snippet-based approach.

The image is regenerated whenever a new user is created or when a user updates 
their avatar or when instructed to do so by authorised users.

This module was developed by Koumbit (http://www.koumbit.org) based on funding 
from the HomelessNation project (http://homelessnation.org).

Initial 4.6 version by Julien Keable (jkeable - http://drupal.org/user/50670).
Port to Drupal 5.0 by David Lesieur (http://drupal.org/user/17157).
Additionnal features, bug fixes and French translation by Patrick Fournier 
(http://drupal.org/user/160468).