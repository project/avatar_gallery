<?php

/**
 * Implementation of hook_menu()
 *
 * @see hook_menu()
 */
function avatar_gallery_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/avatar_gallery',
      'title' => t('Avatar gallery'),
      'description' => t('Configure the avatar gallery.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('avatar_gallery_admin_settings'),
      'access' => user_access('administer site configuration'),
    $items[] = array(
      'path' => 'admin/settings/avatar_gallery/settings',
      'title' => t('Settings'),
      'description' => t('Configure images and text.'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_DEFAULT_LOCAL_TASK),
      'weight' => 0,
    );
    $items[] = array(
      'path' => 'admin/settings/avatar_gallery/view',
      'title' => t('View'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('avatar_gallery_view'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    );
  }
  return $items;
}

/**
 * Implementation of hook_block().
 */
function avatar_gallery_block($op = 'list', $delta = 0) {
  $menu = menu_get_menu();

  if ($op == 'list') {
    $blocks[]=array("info"=>t("Avatar gallery"));
    return $blocks;
  }
  else if ($op == 'view') {
    $data['subject'] = t("Avatar gallery");
    $result = db_fetch_object(db_query("SELECT COUNT(u.uid) as count FROM {users} u WHERE u.status != 0 and u.uid != 0"));
    $members = format_plural($result->count, '1 user', '@count users');
    if (module_exists('profile')) {
      $members = l($members, 'profile');
    }
    $data['content'] .= '<div id="usercount">'.t('There are !members on @sitename.', array('!members' => $members, '@sitename' => variable_get('site_name', 'drupal'))).'</div>';
    $data['content'] .=  _avatar_gallery_imagemap();
    return $data;
  }
}

/**
 * Implementation of hook_user()
 *
 * On user creation or modification, regenerates the gallery
 */
function avatar_gallery_user($op, $edit, $user) {
  switch ($op) {
    case 'update':
    {
      avatar_gallery_regenerate();
      break;
    }
    case 'login':
    {
      $order = variable_get('avatar_gallery_ordering', 0);
      if ($order == 2) {
        avatar_gallery_regenerate();
      }
      break;
    }
    case 'insert':
    case 'delete':
    {
      if (isset($edit["picture"])) {
        avatar_gallery_regenerate();
      }
      break;
    }
  }
}

/**
 * Menu callback to display the avatar gallery.
 *
 * @see _avatar_gallery_imagemap()
 */
function avatar_gallery_view() {
  $form['gallery'] = array(
    '#value' => _avatar_gallery_imagemap(),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Regenerate'),
  );
  
  return $form;
}

function avatar_gallery_view_submit($form_id, $form_values) {
  avatar_gallery_regenerate();
  return "admin/settings/avatar_gallery/view";
}

/**
 * Menu callback for administration settings page.
 */
function avatar_gallery_admin_settings() {
  $form['visual'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['visual']['avatar_gallery_ordering'] = array(
    '#type' => 'select',
    '#title' => t('Ordering'),
    '#description' => t('Ordering of the pictures, left to right, then top to bottom.'),
    '#options' => _avatar_gallery_ordering('strings'),
    '#default_value' => variable_get('avatar_gallery_ordering', '0'),
  );
  $form['visual']['avatar_gallery_num_cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Columns'),
    '#description' => t('Number of pictures in one row of the gallery.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_num_cols', 6),
  );
  $form['visual']['avatar_gallery_num_rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Rows'),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Maximum number of rows in the gallery. Zero means no limit (all users will be listed).'),
    '#default_value' => variable_get('avatar_gallery_num_rows', 0),
  );
  $form['visual']['avatar_gallery_margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Margin'),
    '#description' => t('Margin around the avatar gallery, in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_margin', 25),
  );
  $form['visual']['avatar_gallery_hspacing'] = array(
    '#type' => 'textfield',
    '#title' => t('Column Width'),
    '#description' => t('Column width, in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_hspacing', 100),
  );
  $form['visual']['avatar_gallery_vspacing'] = array(
    '#type' => 'textfield',
    '#title' => t('Row Height'),
    '#description' => t('Row height, in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_vspacing', 100),
  );
  $form['visual']['avatar_gallery_thumb_hsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Width'),
    '#description' => t('Width of the pictures, in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_thumb_hsize', 85),
  );
  $form['visual']['avatar_gallery_thumb_vsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Height'),
    '#description' => t('Height of the pictures, in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_thumb_vsize', 85),
  );
  $form['visual']['avatar_gallery_crop_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Crop Images'),
    '#description' => t('Crop the images so that their aspect ratio matches the one specified by Picture Width and Picture Height, above.'),
    '#default_value' => variable_get('avatar_gallery_crop_images', FALSE),
  );
  $form['visual']['avatar_gallery_quality'] = array(
    '#type' => 'textfield',
    '#title' => t('JPEG Quality'),
    '#description' => t('An integer between 0 and 100; higher values means better looking pictures, but larger gallery size.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_quality', 80),
  );
  $form['visual']['background_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background Color'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['visual']['background_color']['avatar_gallery_bcolr'] = array(
    '#type' => 'textfield',
    '#title' => t('Red'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_bcolr', 255),
  );
  $form['visual']['background_color']['avatar_gallery_bcolg'] = array(
    '#type' => 'textfield',
    '#title' => t('Green'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_bcolg', 255),
  );
  $form['visual']['background_color']['avatar_gallery_bcolb'] = array(
    '#type' => 'textfield',
    '#title' => t('Blue'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_bcolb', 255),
  );

  $form['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['text']['avatar_gallery_showtext'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show User Name'),
    '#description' => t('Show user name under each picture.'),
    '#default_value' => variable_get('avatar_gallery_showtext', 0),
  );
  $form['text']['avatar_gallery_font'] = array(
    '#type' => 'textfield',
    '#title' => t('TrueType Font Path'),
    '#description' => t('Path of the TrueType font to use for the text (e.g. files/arial.ttf).'),
    '#size' => 50,
    '#maxlength' => 80,
    '#default_value' => variable_get('avatar_gallery_font', ''),
  );
  $form['text']['avatar_gallery_textsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#description' => t('Font size in pixels.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_textsize', 15),
  );
  $form['text']['text_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text Color'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['text']['text_color']['avatar_gallery_tcolr'] = array(
    '#type' => 'textfield',
    '#title' => t('Red'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_tcolr', 0),
  );
  $form['text']['text_color']['avatar_gallery_tcolg'] = array(
    '#type' => 'textfield',
    '#title' => t('Green'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_tcolg', 0),
  );
  $form['text']['text_color']['avatar_gallery_tcolb'] = array(
    '#type' => 'textfield',
    '#title' => t('Blue'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('avatar_gallery_tcolb', 0),
  );

  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Missing Picture'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['misc']['avatar_gallery_showall'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show users with no avatars'),
    '#default_value' => variable_get('avatar_gallery_showall', 0),
  );
  $form['misc']['avatar_gallery_miss_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image for missing avatars'),
    '#description' => t('Path of the image to use for users with no picture (e.g. files/avatar.jpg).'),
    '#size' => 50,
    '#maxlength' => 80,
    '#default_value' => variable_get('avatar_gallery_miss_image', ''),
  );
    
  return system_settings_form($form);
}

function _avatar_gallery_ordering($type) {
  if ($type == 'strings') {
    return array(      
      '0' => t('Newest first'),
      '1' => t('By name'),
      '2' => t('Last log in'),
    );

  }
  else if ($type == 'sql') {
    return array(      
      'created DESC',
      'name ASC',
      'login DESC',
    );
  }
}


/**
 * Generate the HTML code necessary to display the image and the area map
 *
 * @args $root string the base url of links
 * @returns string generally an <IMG> tag followed by a <AREA> tag
 */
function _avatar_gallery_imagemap() {
  $img_file = variable_get('file_directory_path', 'files' ) . '/' . variable_get('avatar_gallery_image_name', 'avatar_gallery.jpg');
  $img = theme_image($img_file, t('Avatar gallery'), t('Avatar gallery'), array('border' => 0, 'usemap' => '#avatar_gallery_areamap'), FALSE) . _avatar_gallery_areamap_dyn($root);
  return $img;
}

/**
 * Build an areamap from coordinates cached by avatar_gallery_regenerate()
 *
 * This will call avatar_gallery_regenerate() to recreate the cache if
 * missing or expired
 *
 * @arg $root string the base of urls
 * @return string an <areamap> named avatar_gallery_areamap
 * corresponding to the image created by avatar_gallery_regenerate()
 * @see avatar_gallery_regenerate()
 */
function _avatar_gallery_areamap_dyn($root = '') {
  if (!$cache = cache_get('avatar_gallery_coordinates')) {
    avatar_gallery_regenerate();
    $cache = cache_get('avatar_gallery_coordinates');
  }
  
  $coords = unserialize($cache->data);
  if ($coords) {
    $amap = '';
    foreach ($coords as $uid => $coord) {
      $amap .= '<area shape="rect" coords="' . $coord . '" href="' . $root . url("user/" . $uid) . '">';
    }
    return '<map name="avatar_gallery_areamap">' . $amap . '</map>';
  }
  else {
    return '<map name="avatar_gallery_areamap"/>';
  }
}

/**
 * Regenerate the avatar gallery
 *
 * This is where most of the work is done. here the image is created
 * and the coordinates of the image map are computed and cached as
 * avatar_gallery_coordinates in the cache system.
 *
 * The image is created in $file_directory_path/avatar_gallery_md5(time()).jpg
 */
function avatar_gallery_regenerate()
{
  $order_value = variable_get('avatar_gallery_ordering', 0);
  $order_sql = _avatar_gallery_ordering('sql');
  $order = $order_sql[$order_value];
  
  $miss_image = variable_get('avatar_gallery_miss_image', '');
  $show_miss = variable_get('avatar_gallery_showall', 0) && $miss_image;
  
  $numcols = variable_get('avatar_gallery_num_cols', 6);
  
  $query = 'SELECT uid,name,picture FROM {users} WHERE status != 0 and uid != 0';
  if (!$show_miss) {
   $query .= ' and picture <> \'\'';
  }
  $query .= ' ORDER BY %s';
  
  $row_limit = variable_get('avatar_gallery_num_rows', 0);
  if ($row_limit != 0) {
    $limit = $row_limit * $numcols;
    $query .= ' LIMIT ' . $limit;
  }
  
  $items = array();
  $result = db_query($query, $order);
  while ($account = db_fetch_object($result)) {
    $items[] = $account;
  }

  // create background
  $hspacing = variable_get('avatar_gallery_hspacing', 100);
  $vspacing = variable_get('avatar_gallery_vspacing', 100);
  $margin = variable_get('avatar_gallery_margin', 25);

  $bcolr = variable_get('avatar_gallery_bcolr', 255);
  $bcolg = variable_get('avatar_gallery_bcolg', 255);
  $bcolb = variable_get('avatar_gallery_bcolb', 255);

  $new_w = variable_get('avatar_gallery_thumb_hsize', 85);
  $new_h = variable_get('avatar_gallery_thumb_vsize', 85);
  $crop = variable_get('avatar_gallery_crop_images', FALSE);

  $textsize = variable_get('avatar_gallery_textsize', 15);
  $font = variable_get('avatar_gallery_font', '');
  $showtext = variable_get('avatar_gallery_showtext', 0) && $font;
  $tcolr = variable_get('avatar_gallery_tcolr', 0);
  $tcolg = variable_get('avatar_gallery_tcolg', 0);
  $tcolb = variable_get('avatar_gallery_tcolb', 0);

  $quality = variable_get('avatar_gallery_quality', 80);

  $maxx = $numcols * $hspacing + $margin * 2;
  $maxy = ceil(count($items) / $numcols) * $vspacing + $margin * 2;

  if ($maxy == 0) {
    $maxy = $vspacing;
  }

  $dst_img = ImageCreateTrueColor($maxx, $maxy);
  $bcol = imagecolorallocate($dst_img, $bcolr, $bcolg, $bcolb);
  imagefill($dst_img, 0, 0, $bcol);

  $tcol = imagecolorallocate($dst_img, $tcolr, $tcolg, $tcolb);

  if ($showtext)  {
    $dimensions = imagettfbbox($textsize, 0, $font, 'W');
    $lineHeight = ($dimensions[1] - $dimensions[7]); // the height of a single line
  }

  $colnum = -1;
  $rownum = 0;
  foreach ($items as $k => $v) {

    $picfile = $v->picture;
    if ($picfile != '' || $show_miss) {
      if ($picfile == '' || !file_exists($picfile))  {
        if ($miss_image == '') {
          drupal_set_message(t('The missing image file has not been specified'));
        }
        $picfile = $miss_image;
      }
      $colnum++;
      if ($colnum >= $numcols) {
        $rownum++;
        $colnum=0;
      }

      // we have a picture
      $image_data = @getimagesize($picfile);
      $image_type = $image_data[2];
      if ($image_type == 1) {
        $src_img = imagecreatefromgif($picfile);
      }
      elseif ($image_type == 2) {
        $src_img = imagecreatefromjpeg($picfile);
      }
      elseif ($image_type == 3) {
        $src_img = imagecreatefrompng($picfile);
      }
      else {
        if ($picfile != '') {
          drupal_set_message(t('Avatar Gallery: unsupported image format: @picfile', array('@picfile' => $picfile)));
        }
        continue;
      }

      $old_w = imagesx($src_img);
      $old_h = imagesy($src_img);

      if ($crop) {
        $thumb_w = $new_w;
        $thumb_h = $new_h;
  
        $pic_x = $colnum * $hspacing + $margin;
        $pic_y = $rownum * $vspacing + $margin;
        
        if (($old_w / $old_h) > ($new_w / $new_h)) {
          $w = $new_w * $old_h / $new_h;
          
          $crop_x = ($old_w - $w) / 2;
          $crop_y = 0;
          
          $old_w = $w;
        }
        else if (($old_w / $old_h) < ($new_w / $new_h)) {
          $h = $new_h * $old_w / $new_w;
          
          $crop_x = 0;
          $crop_y = ($old_h - $h) / 2;
          
          $old_h = $h;
        }
        else if (($old_w / $old_h) == ($new_w / $new_h)) {
          $crop_x = 0;
          $crop_y = 0;
        }
      }
      else {
        if (($old_w / $old_h) > ($new_w / $new_h)) {
          $thumb_w = $new_w;
          $thumb_h = $old_h * ($new_w/$old_w);
        }
        else if (($old_w / $old_h) < ($new_w / $new_h)) {
          $thumb_w = $old_w * ($new_h/$old_h);
          $thumb_h = $new_h;
        }
        else if (($old_w / $old_h) == ($new_w / $new_h)) {
          $thumb_w = $new_w;
          $thumb_h = $new_h;
        }
  
        $maxs = max($thumb_w, $thumb_h);
  
        $pic_x = ($maxs-$thumb_w)/2 + $colnum * $hspacing + $margin;
        $pic_y = ($maxs-$thumb_h)/2 + $rownum * $vspacing + $margin;
        
        $crop_x = 0;
        $crop_y = 0;
      }
      
      imagecopyresampled(
        $dst_img, $src_img,
        $pic_x, $pic_y,
        $crop_x, $crop_y,
        $thumb_w, $thumb_h,
        $old_w, $old_h);

      $coords[$v->uid] = $pic_x.",".$pic_y.",".($pic_x+$thumb_w).",".($pic_y+$thumb_h);

      // now write name of user under picture
      if ($showtext) {
        $dimensions = imagettfbbox($textsize, 0, $font, $v->name);
        $lineWidth = $dimensions[2] - $dimensions[0]; // get the length of this line, if the word is to be included

        $leftStart=-$lineWidth/2;
        imagettftext(
          $dst_img,
          $textsize,
          0,
          $colnum * $hspacing + $margin + $new_w / 2 + $leftStart,
          $rownum * $vspacing + $margin + $new_h + $lineHeight,
          $tcol,
          $font,
          $v->name);
      }

      // destroy thumbnail
      imagedestroy($src_img);
    }
  }
  
  $current_image_path = variable_get('file_directory_path', 'files') . '/' . variable_get('avatar_gallery_image_name', 'avatar_gallery.jpg');
  
  $new_image_name = 'avatar_gallery_' . md5(time()) . '.jpg';
  imagejpeg($dst_img, variable_get('file_directory_path', 'files') . '/' . $new_image_name, $quality);
  variable_set('avatar_gallery_image_name', $new_image_name);
  
  if (file_exists($current_image_path)) {
    file_delete($current_image_path);
  }

  imagedestroy($dst_img);

  cache_clear_all('avatar_gallery_coordinates', 'cache');
  cache_set('avatar_gallery_coordinates', 'cache', serialize($coords), CACHE_PERMANENT);
}

?>
